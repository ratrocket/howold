package main

import "testing"

func TestHuman(t *testing.T) {
	cc := []struct {
		seconds  int64
		expected string
	}{
		// S
		{0, "0s"},
		{1, "1s"},
		{58, "58s"},
		{59, "59s"},
		// M S
		{60, "1m 0s"},
		{61, "1m 1s"},
		{123, "2m 3s"},
		{1215, "20m 15s"},
		{3599, "59m 59s"},
		// H M S
		{3600, "1h 0m 0s"},
		{55267, "15h 21m 7s"},
		{54000 + 1260 + 7, "15h 21m 7s"}, // same as above...
		{86399, "23h 59m 59s"},
		// D H M
		{86400, "1d 0h 0m"},
		{432000 + 43200 + 2220, "5d 12h 37m"},
		{432000 + 43200 + 2220 + 32, "5d 12h 37m"}, // as above with extra seconds
		{2505600 + 82800 + 3540, "29d 23h 59m"},
		{2505600 + 82800 + 3540 + 59, "29d 23h 59m"},
		// Mo D
		{2505600 + 82800 + 3540 + 60, "1mo 0d"},
		{2592000, "1mo 0d"},
		{2592000 + 1, "1mo 0d"},
		{2592000 + 86399, "1mo 0d"}, // can add up to 24 hours minus 1 second
		{2592000 + 86400, "1mo 1d"}, // adding the 1 second tips it over.
		{2592000 + 1296000, "1mo 15d"},
		{2592000 + 1296000 + 82800, "1mo 15d"}, // add 23 hours to above, same output
		{28512000 + 2505600, "11mo 29d"},
		{28512000 + 2505600 + 1, "11mo 29d"},
		{28512000 + 2505600 + 86399, "11mo 29d"},
		// Y Mo D
		{28512000 + 2505600 + 86400, "1y 0mo 0d"},
		{31104000, "1y 0mo 0d"},
		{31104000 + (24*60*60 - 1), "1y 0mo 0d"}, // add 23:59:59 to the above; same result
		{31104000 + (30 * 24 * 60 * 60) + 1, "1y 1mo 0d"},
	}

	for _, c := range cc {
		actual := human(c.seconds)
		if actual != c.expected {
			t.Errorf("human(%d) = %q; expected %q", c.seconds, actual, c.expected)
		}
	}
}
