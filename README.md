How old is this file (or files)?

> **(May 2021)** moved to [md0.org/howold](https://md0.org/howold).

```
howold <file> [<files> ...]
```
