// Howold tells you how old a file is.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"time"
)

const VERSION = "0.0.1"

func usage() {
	fmt.Fprintf(os.Stderr, "usage: howold <file> [<files> ...]\n")
	os.Exit(2)
}

func main() {
	var (
		v = flag.Bool("v", false, "print version")
	)
	log.SetPrefix("howold: ")
	flag.Usage = usage
	flag.Parse()
	if *v {
		fmt.Println(VERSION)
		return
	}
	if flag.NArg() == 0 {
		flag.Usage()
	}
	now := time.Now()
	for _, arg := range flag.Args() {
		fi, err := os.Lstat(arg)
		if err != nil {
			log.Print(err)
			continue
		}
		fmt.Printf("%s: %s\n", fi.Name(), h(now, fi.ModTime()))
	}
}

// H simply combines durationToSeconds() with human() to make the call
// in main() a little nicer... Totally unnecessary.
func h(now, filetime time.Time) string {
	return human(durationToSeconds(now.Sub(filetime)))
}

// DurationToSeconds converts a time.Duration to seconds.
// This is separated from human() to make testing human() easier.
func durationToSeconds(d time.Duration) int64 {
	return int64(d) / 1000000000
}

// Human converts a number of seconds to a human readable form.  The
// conversion is approximate, especially due to defining a "month" as 30
// days and a "year" as 12 of those "months" (so 12 * 30 days).  And of
// course it's not accurate across leap seconds and things like that.
// Also, at longer time intervals, smaller time units are thrown away
// (eg, if the time is over a day but less than a "month", the seconds
// aren't displayed; if over a year, only "months" and days are
// displayed).
func human(seconds int64) string {
	var (
		minute int64 = 60
		hour   int64 = minute * 60
		day    int64 = hour * 24
		month  int64 = day * 30   // clearly this isn't accurate to the day
		year   int64 = month * 12 // ditto
	)

	if seconds < minute {
		// S
		return fmt.Sprintf("%ds", seconds)
	} else if seconds < hour {
		// M S
		return fmt.Sprintf("%dm %ds", seconds/minute, seconds%minute)
	} else if seconds < day {
		// H M S
		return fmt.Sprintf("%dh %dm %ds", seconds/hour, (seconds%hour)/minute, (seconds%hour)%minute)
	} else if seconds < month {
		// D H M
		return fmt.Sprintf("%dd %dh %dm", seconds/day, (seconds%day)/hour, ((seconds%day)%hour)/minute)
	} else if seconds < year {
		// Mo D
		return fmt.Sprintf("%dmo %dd", seconds/month, (seconds%month)/day)
	} else { // >= year
		// Y Mo D
		return fmt.Sprintf("%dy %dmo %dd", seconds/year, (seconds%year)/month, ((seconds%year)%month)/day)
	}
	return "" // unreachable
}
